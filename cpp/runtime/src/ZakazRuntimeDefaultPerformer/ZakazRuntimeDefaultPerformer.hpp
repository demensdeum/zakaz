#include <Performer.hpp>

using namespace ZakazRuntime;

class ZakazRuntimeDefaultPerformer: public Performer {

public:
	bool canHandleLine(string line);
	void performLine(string line);

	virtual ~ZakazRuntimeDefaultPerformer();

};