#include "ZakazRuntimeDefaultPerformer.hpp"
#include <regex>

using namespace std;

bool ZakazRuntimeDefaultPerformer::canHandleLine(string line) {

	auto regexString = std::regex("Show \"(.*)\" text on screen");

	return regex_search(line, regexString);
};

void ZakazRuntimeDefaultPerformer::performLine(string line) {

	auto regexString = std::regex("Show \"(.*)\" text on screen");

	smatch match;

	regex_search(line, match, regexString);

	auto textToShow = match.str(1);

	cout << textToShow << endl;
};

ZakazRuntimeDefaultPerformer::~ZakazRuntimeDefaultPerformer() {
};