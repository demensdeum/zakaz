#include <algorithm>
#include <iostream>
#include <dlfcn.h>
#include <fstream>
#include <vector>
#include <memory>
#include <sstream>
#include "dlfcn.h"
#include "Performer.hpp"
#include "ZakazRuntimeDefaultPerformer/ZakazRuntimeDefaultPerformer.hpp"

using namespace std;

typedef shared_ptr<Performer> (*SharedPtrPerformerFunctionPointer)();	

class IsPerformerCanHandleLinePredicate
{
    public:
	IsPerformerCanHandleLinePredicate(string line) {
		this->line = line;
	}

	bool operator()(const shared_ptr<Performer> performer) const {
            return performer->canHandleLine(line);
        }

	private:
		string line;
};

int main (int argc, char *argv[]) {

	cout << "Zakaz Runtime by demensdeum@gmail.com v1.0  (2019)" << endl;

	if (argc < 2) {
		cout << "Usage: zakaz helloWorld.tez" << endl;
		cout << "Usage: zakaz helloWorld.tez ZakazRuntimeDefaultPerformer, FSEGTPerformer" << endl;
		exit(1);
	}

	auto performers = vector<shared_ptr<Performer>>();

	auto defaultPerformer = make_shared<ZakazRuntimeDefaultPerformer>();
	performers.push_back(defaultPerformer);

	if (argc == 3) {
		string performersString = argv[2];
		cout << "Performers string: " << performersString << endl;

		auto stringStream = istringstream(performersString);

		string performerString;
		while (stringStream >> performerString) {
    			cout << performerString << endl;

			string performerLibPath = "./";
			performerLibPath += performerString;
			performerLibPath += ".so";

			auto extensionHandle = dlopen(performerLibPath.c_str(), RTLD_LAZY);
			if (!extensionHandle) {
				string errorString = dlerror();
				throw runtime_error(errorString);
			}

			auto functionPointer = SharedPtrPerformerFunctionPointer();
			functionPointer = (SharedPtrPerformerFunctionPointer) dlsym(extensionHandle, "createPerformer");
			auto dlsymError = dlerror();
 			if (dlsymError) {
				string errorString = dlerror();
				throw runtime_error(errorString);
 			}

			auto performer = functionPointer();
			performers.push_back(performer);

			dlclose(extensionHandle);
		}
	}

	auto filePath = argv[1];

	ifstream fileStream(filePath);	

	string line;
	while (std::getline(fileStream, line))
	{
		auto iterator = find_if (performers.begin(), performers.end(), IsPerformerCanHandleLinePredicate(line));
		if (iterator != performers.end()) {
			iterator->get()->performLine(line);
		}
		else {
			cout << "Performer for line: \"" << line << "\" not found... exit" << endl;
			exit(2);
		}
	}

	exit(0);
} 
