#ifndef FIREFOX_PERFORMER_H_
#define FIREFOX_PERFORMER_H_

#include <Performer.hpp>

using namespace ZakazRuntime;

class FirefoxPerformer: public Performer {

public:
	bool canHandleLine(string line);
	void performLine(string line);

	virtual ~FirefoxPerformer();

};

#endif