#include "FirefoxPerformer.hpp"
#include <regex>
#include <stdlib.h>

using namespace std;

typedef shared_ptr<Performer> (*SharedPtrPerformerFunctionPointer)();

bool FirefoxPerformer::canHandleLine(string line) {

	auto regexString = std::regex("Show website with address \"(.*)\" in Firefox");

	return regex_search(line, regexString);
};

void FirefoxPerformer::performLine(string line) {

	auto regexString = std::regex("Show website with address \"(.*)\" in Firefox");

	smatch match;

	regex_search(line, match, regexString);

	auto textToShow = match.str(1);

	string firefoxString = "firefox ";
	firefoxString += textToShow;

	cout << firefoxString << endl;

	system(firefoxString.c_str());
};

FirefoxPerformer::~FirefoxPerformer() {
};