#include "FirefoxPerformer.hpp"
#include <memory>

using namespace std;

extern "C" shared_ptr<FirefoxPerformer> createPerformer() {
	auto firefoxPerformer = make_shared<FirefoxPerformer>();
	return firefoxPerformer;
};