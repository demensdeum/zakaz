#ifndef ZAKAZ_RUNTIME_H_
#define ZAKAZ_RUNTIME_H_

#include <iostream>

using namespace std;

namespace ZakazRuntime {
class Performer {

public:
	virtual bool canHandleLine(string line) = 0;
	virtual void performLine(string line) = 0;
};
};
#endif